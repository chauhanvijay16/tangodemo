def handler(context, inputs):
    greeting = "Hi there, {0}!".format(inputs["target"])
    print(greeting)

    outputs = {
      "greeting": greeting
    }

    return outputs
